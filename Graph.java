/**
 * Package backtracking :  
 * Demonstration zur Vererbung: Klassen Person, Student, Dozent
 * fhdwbap/2019
 */
package backtracking;

/**
 * Modellierung eines Graphen für den Kontext des Wolf-Ziege-Kohlkopf-Beispiels.
 * @author fhdwbap
 *
 */
public class Graph 
{
	/*
	 * Das Array der Knoten-Referenzen
	 */
    private Vertice[] va;
    
	public Graph()
	{
		va = new Vertice[BTMain.MAXVERTICES];
		
		// Zunächst null-Initialisierung
		for (int i=0; i<va.length; i++)
		{
			va[i] = null;
		}
		
		init(); // interne, ausgelagerte Methode: hier werden die Verbindungen
		// des Graphen aufgebaut.
	}
	
	/*
	 * Abruf des k-ten Knotens
	 */
	public Vertice getV(int k)
	{
		return va[k];
	}
	
	/*
	 * Prüfung auf den leeren Graphen 
	 */
	public boolean isEmpty()
	{
		for (int i=0; i<BTMain.MAXVERTICES; i++)
		{
			if (va[i] != null)
			{
				return false;
			}
		}
		return true;
	}
	
	/*
	 * Textbasierte Ausgabe des Graphen
	 */
	public void show()
	{
		System.out.println("Ausgabe des Graphen.");
		for (int i=0; i<va.length; i++)
		{
			if (va[i] != null)
			{
				System.out.print("Pos. " + i + ": ");
				va[i].show();
			}
		}
		System.out.println("Ende der Ausgabe des Graphen.");
		
	}

	/*
	 * Hilfsmethode: Prüfung, ob ein wert in einem Array vorkommt
	 */
	private boolean in(int wert, int[] values)
	{
		for (int value : values)
		{
			if (wert == value)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Initialisieren des Graphes - findet über den Konstruktor statt, der init() aufruft 
	 */
	private void init()
	{
		int used[] = { 0, 1, 2, 4, 5, 10, 11, 13, 14, 15, -1 };
		
		for (int i=0; i<BTMain.MAXVERTICES; i++)
		{
			if (in(i,used))
			{
				va[i] = new Vertice( i );
			}
		}
		
		// Nun wird der konkrete Graph zusammengebaut, pv[0] entspricht
		// dabei dem Startknoten "0000", pv[10] entspricht "1010" usw.
		
		va[0].setNeighbour(0,va[10]);
		va[10].setNeighbour(0,va[2]);
		va[2].setNeighbour(0,va[14]);
		va[2].setNeighbour(1,va[11]);
		va[14].setNeighbour(0,va[4]);
		va[4].setNeighbour(0,va[13]);
		va[11].setNeighbour(0,va[1]);
		va[1].setNeighbour(0,va[13]);
		va[13].setNeighbour(0,va[5]);
		va[5].setNeighbour(0,va[15]);
		// Hier wurden der Uebersichtlichkeit halber nur unidirektional
		// die "zielfuehrenden" Verbindungen eingetragen.		
		
	} // end init()
	
} // end class Graph 



